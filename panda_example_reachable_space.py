import roboticstoolbox as rp
import numpy as np
from roboticstoolbox.tools import URDF, xacro
import time

from scipy.spatial import ConvexHull

panda = rp.models.DH.Panda()
# inital
q0= np.array([0.00 , 0, 0,   -1.6, -6.64181e-05,    1.56995,0])
# singular
#q0= np.array([0.0 ,1.08, 0.59,  -0.25, -0.56,   3.23, 0])
# some 
#q0= np.array([-0.77, 0.88, 0.40,-0.97,1.21,3.23,00])
panda.q = q0


# get the robot actuation and kimeatic limits
# joint space limits
q_min, q_max  = panda.qlim
dq_max = rp.models.URDF.Panda().qdlim[:-2]
dq_min = -dq_max
t_max = np.array([87, 87, 87, 87, 20, 20, 20]) 
t_min = -t_max

# cartesian space limits
ddx_max = np.array([13.0,13,13])
ddx_min = -ddx_max
dx_max = np.array([1.7,1.7,1.7])
dx_min = -dx_max


# object mass
m_o = 0
# prediction horizon
delta_t = 0.15


# implemenations of 
from reachability_utils import reachable_set_polytope, reachable_set_cube, robot_simulation

# polytope P_x calculation
vertex_Px, faces_Px, t_vert_Px, H_Px, d_Px = reachable_set_polytope(panda, q0, q_max,q_min,dq_max,dq_min,t_max,t_min,delta_t,m_o)

# cube C_x calculation
vertex_Cx, faces_Cx, H_Cx, d_Cx = reachable_set_cube(panda, q0, dx_min, dx_max, ddx_min, ddx_max, delta_t, m_o)

# robot simulation
x_robot = robot_simulation(panda, q0, t_vert_Px.T, q_max,q_min,dq_max,dq_min,t_max,t_min,delta_t,m_o)


# check the accuracy metrics
print("Accuracy analysis for polytope $P_x$")
N_sim = len(x_robot)
n = 0
x_in = []
for x in x_robot:
    if np.all(H_Px.dot(x - panda.fkine(panda.q).t) <= d_Px) :
        n = n + 1
        x_in.append(x)
print("{} simulation steps inside out of {}.\nMertic m_1 : {:.2f} %".format(n,N_sim,n/N_sim*100))

if x_in:
    Px_v = ConvexHull(vertex_Px.T).volume
    R1_v = ConvexHull(x_in).volume
    R2_v = ConvexHull(x_robot).volume
    print("Volume difference m_2: {:.2f} %, m_3: {:.2f} %".format(R1_v/Px_v*100, Px_v/R2_v*100))

print("Accuracy analysis for cube $C_x$")
n = 0
x_in = []
for x in x_robot:
    if np.all(H_Cx.dot(x - panda.fkine(panda.q).t) <= d_Cx) :
        n = n + 1
        x_in.append(x)
print("{} simulation steps inside out of {}.\nMertic m_1 : {:.2f} %".format(n,N_sim,n/N_sim*100))

if x_in:
    Px_v = ConvexHull(vertex_Cx.T).volume
    R1_v = ConvexHull(x_in).volume
    R2_v = ConvexHull(x_robot).volume
    print("Volume difference m_2: {:.2f} %, m_3: {:.2f} %".format(R1_v/Px_v*100, Px_v/R2_v*100))

# plotting the polytope
import matplotlib.pyplot as plt
from pycapacity.visual import plot_polytope_faces, plot_polytope_vertex # pycapacity visualisation tools

fig = panda.plot(q0)
ax = fig.ax

# draw faces and vertices
plot_polytope_vertex(ax=ax, vertex=vertex_Px, label='polytope $P_x$',color='blue')
plot_polytope_faces(ax=ax, faces=faces_Px, face_color='blue', edge_color='blue', alpha=0.2)

# draw faces and vertices
plot_polytope_vertex(ax=ax, vertex=vertex_Cx, label='cube $C_x$',color='orange')
plot_polytope_faces(ax=ax, faces=faces_Cx, face_color='orange', edge_color='orange', alpha=0.2)

# drawing the simulated points
plot_polytope_vertex(ax=ax, vertex=x_robot.T, color='red', label='simulated')

plt.tight_layout()
plt.legend()
ax.set_xlim([-1, 1.5])
ax.set_ylim([-1, 1.5])
ax.set_zlim([0, 1.5])
plt.show()
fig.hold()