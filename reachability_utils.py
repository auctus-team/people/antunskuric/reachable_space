import numpy as np
import time

import roboticstoolbox as rp

# polytope python module
import pycapacity.robot as robot
import pycapacity.algorithms as algos

def reachable_set_polytope(panda, q0, q_max,q_min,dq_max,dq_min,t_max,t_min,delta_t,m_o):
    # jacobian
    J = panda.jacob0(q0)
    Jac = J[:3,:]
    # mass matrx
    M = panda.inertia(q0)
    M_inv = np.linalg.pinv(M)
    
    # calculate the bias torque (the gravity vecotr + compensation of the mass of the object)
    tau = panda.rne(q0, np.zeros((panda.n,)), np.zeros((panda.n,)))
    gravity = panda.gravload(q0).reshape((-1,1))
    t_bias = Jac.T.dot([0,0,9.81*m_o])+ panda.gravload(q0)

    s = time.time()
    vertex, H,d, faces_index, t_vert, x_vert =  algos.iterative_convex_hull_method(
        A = np.eye(3),
        B = np.dot(Jac, M_inv)*delta_t**2/2,
        y_max=t_max-t_bias, 
        y_min = t_min-t_bias,
        G_in = np.vstack((M_inv*delta_t, -M_inv*delta_t, M_inv*delta_t**2/2, -M_inv*delta_t**2/2)),
        h_in = np.hstack((dq_max.flatten(),-dq_min.flatten(), (q_max.flatten()-q0),-(q_min.flatten()-q0))),
        tol = 0.001,
        max_iter=500)

    vertex = vertex + panda.fkine(q0).t[:, None]
    faces = robot.face_index_to_vertex(vertex,faces_index)
    print("polytope P_x calculation time: {} sec".format(str(time.time()-s)))
    
    return vertex, faces, t_vert, H, d

def reachable_set_cube(panda, q0, dx_min, dx_max, ddx_min, ddx_max, delta_t, m_o):
    
    s = time.time()
    vertex, H,d, faces_index, t_vert, x_vert =  algos.iterative_convex_hull_method(
        A = np.eye(3),
        B = np.eye(3)*delta_t**2/2,
        y_max = ddx_max, 
        y_min = ddx_min,
        G_in = np.vstack((np.eye(3)*delta_t, -np.eye(3)*delta_t)),
        h_in = np.hstack((dx_max, -dx_min)),
        tol = 0.001,
        max_iter=500)

    vertex = vertex + panda.fkine(q0).t[:, None]
    faces = robot.face_index_to_vertex(vertex,faces_index)
    print("cube C_x calculation time: {} sec".format(str(time.time()-s)))
    
    return vertex, faces, H, d

def robot_simulation(panda, q0, t_array, q_max,q_min,dq_max,dq_min,t_max,t_min,delta_t,m_o):
    s = time.time()
    # non-linear robot simulation
    # with robot constraints
    # extended to account for payload influence
    Jac = panda.jacob0(q0)[:3,:]
    t_bias = Jac.T.dot([0,0,9.81*m_o])+ panda.gravload(q0)
    x_end = []
    for t in t_array:
        q_k = q0
        dq_k = np.zeros(panda.n)
        dt = 0.005
        for t_k in np.arange(0,delta_t, dt):
            c_k = panda.coriolis(q_k, dq_k)
            M_k = panda.inertia(q_k)
            g_k = panda.gravload(q_k) 
            ddq_k = np.linalg.pinv(M_k).dot(t - c_k.dot(dq_k) + t_bias - g_k -panda.jacob0(q_k)[:3,:].T.dot([0,0,9.81*m_o]))
            dq_k = ddq_k*dt + dq_k 
            dq_k = np.clip(dq_k,dq_min,dq_max)
            q_k = ddq_k/2*dt**2 + dq_k*dt + q_k 
            q_k = np.clip(q_k,q_min,q_max)
            x_end = algos.stack(x_end, panda.fkine(q_k).t,'v')

    print("robot simulation time: {} sec".format(str(time.time()-s)))
    return x_end