# Approximating robot reachable space using convex polytopes
<i>by Antun Skuric, Vincent Padois, David Daney</i><br>
Accepted to: *15th International Workshop on Human-Friendly Robotics*

<img src="imgs/output1.gif" height="200" />
<img src="imgs/output11.gif" height="200" />
<img src="imgs/output.gif" height="200" />
<img src="imgs/output2.gif" height="200" />

## Download the code

#### By using the terminal:

```bash
git --recursive clone https://gitlab.inria.fr/auctus-team/people/antunskuric/papers/reachable_space.git
```
#### By zip download
Download this repository amd unzip it from the link 
https://gitlab.inria.fr/auctus-team/people/antunskuric/papers/reachable_space.git

## Jupyter notebook
### Installing the anaconda environment
The simplest way to install these python libraries is using anaconda.
```bash
conda env create -f env.yaml    # create the new environemnt and install robotics toolbosx, pycapacity, ...
conda actiavte reachable_sapce
```

#### Creating a custom envirnoment
You can also simply use anaconda to create a new custom environment:
```bash
conda create -n reachable_space python=3.8 pip # create python 3.8 based environment
conda activate reachable_space
conda install -c conda-forge roboticstoolbox-python numpy matplotlib jupyter
```

Then install `pycapacity` for the workspace analysis
```bash
pip install pycapacity
```

For interactive notebook install `tkinter` package as well
```bash
pip install tk
```

### Running the examples
Python script comparing polytope reachable space approximation of the  polytope $P_x$, cube $C_x$ and the real simualted robot reached points $x\in X$

```bash
python panda_example_reachable_space.py
```
Interactive jupyter notebook for easier interaction with the code:
```bash
jupyter notebook panda_reachable_space_notebook.ipynb
```


## ROS catkin workspace

`panda_reachable_space_ws` folder is an example implementation of the reachable space polytope evaluation for Franka Emika Panda robot in a form of a catkin ros worksapce. The directory consists of two ros packages:

- franka_description: Panda robot definitions from Franka Emika  http://wiki.ros.org/franka_description
- **panda_reachable_space: the reachable space solver for Panda robot**

It uses the library [pinocchio](https://github.com/stack-of-tasks/pinocchio) for calculating robot kinematics.
### Dependencies
For visualizing the polytopes in RVIZ you will need to install the [jsk-rviz-plugin](https://github.com/jsk-ros-pkg/jsk_visualization)

```sh
sudo apt install ros-*-jsk-rviz-plugins # melodic/kinetic... your ros version
```

The code additionally depends on the pinocchio library. 

#### Pinocchio already installed
If you do already have pinocchio installed simply install the the pip package `pycapacity`
```
pip install pycapacity
```
And you're ready to go!

#### Installing pinocchio

If you dont have pinocchio installed we suggest you to use Anaconda. Create the environment will all dependencies:

```bash
conda env create -f env.yaml 
conda activate panda_reachable_space  # activate the environment
```

Once the environment is activated you are ready to go.


### One panda simulation
Once when you have everything installed you will be able to run the interactive simulations and see the polytope being visualised in real-time.


<img src="imgs/output_simple1.gif" height="400px"/>

To see a simulation with one panda robot and its force and velocity manipulatibility ellipsoid and polytope run the command in the terminal.

```shell
source panda_reachable_space_ws/devel/setup.bash 
roslaunch panda_reachable_space one_panda.launch
```

> If using anaconda, don't forget to call <br>
> `conda activate panda_reachable_space` <br> 
> before you call `roslaunch`